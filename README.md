# Forest
You are in the forest. Find your way out. Good luck.

[nc task.mtuci.ru 8888]

# Difficulty
medium

# Type
PWN

# Pre-start

`8888` - NCAT FOR BINARY, CHANGE IN COMPOSE

Binary: **./app/forest**

But, there's compilation script **./run.sh** which contains gcc command to build

# Launch

    # docker-compose build
    # docker-compose up

# Solve
app/exploit.py

# Flag
MCTF{N0-gadg3ts-n0-pr0blem0}

# P.S.
nothing
